//
//  AppDelegate.swift
//  ועד סלקום
//
//  Created by Nimit Bagadiya on 8/12/19.
//  Copyright © 2019 Nimit Bagadiya. All rights reserved.
//
import UIKit
import UserNotifications
import UserNotificationsUI
import OneSignal

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,OSSubscriptionObserver {

    var window: UIWindow?
                
                                  
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.setUpNotification(application: application)
        self.setupOneSignal(launchOptions: launchOptions)
      //  OneSignal.add(self as OSSubscriptionObserver)
     
        if let notification = launchOptions?[.remoteNotification] as? [String: AnyObject] {
            let aps = notification["aps"] as! [String: AnyObject]
            print(aps)
        }
                 
        let _: OSHandleNotificationReceivedBlock = { notification in
                     
            print("Received Notification: \(notification!.payload.notificationID ?? "")")
        }
        
        let _: OSHandleNotificationActionBlock = { result in
            // This block gets called when the user reacts to a notification received
            let payload: OSNotificationPayload = result!.notification.payload
            
            var fullMessage = payload.body
            print("Message = \(fullMessage ?? "")")
            
            if payload.additionalData != nil {
                if payload.title != nil {
                    let messageTitle = payload.title
                    print("Message Title = \(messageTitle!)")
                }
                       
                let additionalData = payload.additionalData
                if additionalData?["actionSelected"] != nil {
                    fullMessage = fullMessage! + "\nPressed ButtonID: \(additionalData!["actionSelected"] as? String ?? "")"
                }
            }
        }
    return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

    private func setupOneSignal (launchOptions : [UIApplication.LaunchOptionsKey: Any]?){
        
        let onesignalInitSettings = [kOSSettingsKeyAutoPrompt: false]
        OneSignal.setRequiresUserPrivacyConsent(true)
        OneSignal.consentGranted(true)
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: Constant.OnseSignal.appId,
                                        handleNotificationAction: nil,
                                        settings: onesignalInitSettings)
        
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        OneSignal.promptForPushNotifications(userResponse: { accepted in
            print("User accepted notifications: \(accepted)")
        })
        self.printUserInfo()
    }
    
    func printUserInfo (){
        OneSignal.getPermissionSubscriptionState()
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        
        let hasPrompted = status.permissionStatus.hasPrompted
        print("hasPrompted = \(hasPrompted)")
        let userStatus = status.permissionStatus.status
        print("userStatus = \(userStatus)")
        
        let isSubscribed = status.subscriptionStatus.subscribed
        print("isSubscribed = \(isSubscribed)")
        let userSubscriptionSetting = status.subscriptionStatus.userSubscriptionSetting
        print("userSubscriptionSetting = \(userSubscriptionSetting)")
        let userID = status.subscriptionStatus.userId
//        status.to.userId
        print("userID = \(userID ?? "")")
        let pushToken = status.subscriptionStatus.pushToken
        print("userID = \(pushToken ?? "")")
        //if let string = String(bytes: pushToken, encoding: .utf8) {

       // let token = pushToken.map { String(format: "%.2hhx", $0) }.joined()

        
//        let tokenParts = pushToken.token.map { data in String(format: "%02.2hhx", data) }
//        let token = tokenParts.joined()
//        print("Device Token: \(token)")

        
//        let deviceTokenString = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
//        print(deviceTokenString)
//
//        print("pushToken = \(pushToken ?? "")")
        
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(deviceTokenString)
//        Messaging.messaging().apnsToken = deviceToken
    }

                                                                    
    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
       if !stateChanges.from.subscribed && stateChanges.to.subscribed {
          print("Subscribed for OneSignal push notifications!")
       }
        print("SubscriptionStateChange: \n\(String(describing: stateChanges))")

       //The player id is inside stateChanges. But be careful, this value can be nil if the user has not granted you permission to send notifications.
       if let playerId = stateChanges.to.userId {
          print("Current playerId \(playerId)")
        
       }
    }
                                                            
    
//    func onOSSubscriptionChanged(_ stateChanges: OSSubscriptionStateChanges!) {
//       if !stateChanges.from.subscribed && stateChanges.to.subscribed {
//          print("Subscribed for OneSignal push notifications!")
//          // get player ID
//        print(stateChanges.to.userId ?? "")
//       }
//       print("SubscriptionStateChange: \n\(stateChanges)")
//    }

    
    private func setUpNotification (application : UIApplication) {
        if #available(iOS 10, *) {
            
            UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
                guard error == nil else {
                    return
                }
                if granted {
                   DispatchQueue.main.async {
                        application.registerForRemoteNotifications()
                        UNUserNotificationCenter.current().delegate = self
                    }
                }
                else {
                }
            }
            application.registerForRemoteNotifications()
        }
        else {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
    }
    
    // MARK:- NOtification Delegate
    
//    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
//        print("APNs device token: \(deviceTokenString)")
//    }
    
    // Called when APNs failed to register the device for push notifications
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        completionHandler(.newData)
    }
    
}


extension AppDelegate : UNUserNotificationCenterDelegate{
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert,.sound,.badge])
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let application = UIApplication.shared
        if(application.applicationState == .active){
            print("user tapped the notification bar when the app is in foreground")
        }
        if(application.applicationState == .inactive){
            print("user tapped the notification bar when the app is in background")
        }
        
        if let dictUserInfo = response.notification.request.content.userInfo as? [String:Any]{
            print(dictUserInfo)
            if let dictCustom = dictUserInfo["custom"] as? [String:Any]{
                print(dictCustom)
                if let dictA = dictCustom["a"] as? [String:Any]{
                    print(dictA)
                    if let urlStr = dictA["myappurl"] as? String{
                        if let url = URL(string: urlStr){
                            print(url)
                            if let controller = self.window?.rootViewController as? WebViewViewController{
                                DispatchQueue.main.async {
                                    controller.redirectWebViewToURL(url: url)
                                }
                            }
                        }
                    }
                }
            }
        }
        completionHandler()
    }
}
