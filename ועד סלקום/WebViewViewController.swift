//
//  WebViewViewController.swift
//  ועד סלקום
//
//  Created by Nimit Bagadiya on 8/12/19.
//  Copyright © 2019 Nimit Bagadiya. All rights reserved.
//

import UIKit
import WebKit
import OneSignal

class WebViewViewController: UIViewController {

    // MARK:- IBOutlet
    
    @IBOutlet weak var webView: UIWebView!
    
    // MARK:- Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        // Do any additional setup after loading the view.
    }
    
    // MARK:- Setup
    
    private func setupView (){
        webView.delegate = self
        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
        let strUrl = "\(Constant.HostURL.url)?osUserId=\(status.subscriptionStatus.userId ?? "")"
        if let url = URL(string: strUrl){
            let request = URLRequest(url: url)
            webView.loadRequest(request)
        }
    }     
                        
    func redirectWebViewToURL (url :  URL){
        let request = URLRequest(url: url)
        webView.loadRequest(request)
    }
                    
}
extension WebViewViewController : UIWebViewDelegate{
    
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
    
        
        if let urlString = request.url?.absoluteString{

            if urlString.contains("https://www.vaadcellcom.co.il/me-2/") || urlString.contains("https://www.vaadcellcom.co.il/wp-admin/") || urlString.contains("https://www.vaadcellcom.co.il/me/"){
                if urlString.contains("osUserId=") == false{
                    if urlString.contains("?"){
                        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
                        let str = "\(urlString)&osUserId=\(status.subscriptionStatus.userId ?? "")"
                        if let url = URL(string: str){
                            let request = URLRequest(url: url)
                            webView.loadRequest(request)
                            return false
                        }
                    }else{
                        let status: OSPermissionSubscriptionState = OneSignal.getPermissionSubscriptionState()
                        let str = "\(urlString)?osUserId=\(status.subscriptionStatus.userId ?? "")"
                        if let url = URL(string: str){
                            let request = URLRequest(url: url)
                            webView.loadRequest(request)
                            return false
                        }
                    }
                }
            }
            

        }
        print("Loading...\(request.url?.absoluteString ?? "")")
        return true
    }
}

